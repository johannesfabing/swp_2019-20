package at.johfab.cduebung;

import java.util.ArrayList;
import java.util.List;

public class Song extends A_Playable {
	
	private List<CD> CDs;
	
	public Song(String title, int length) {
		super();
		this.title = title;
		this.length = length;
		this.CDs = new ArrayList<CD>();
	}
	
	public void play() {
		System.out.println("Titel: "+title);
		System.out.println("L�nge: "+length);
	}
	
	public void addCD(CD cd) {
		CDs.add(cd);
	}
	
	public void displayAlbums() {
		for(CD c : CDs){
			System.out.println(c);
		}
	}
}
