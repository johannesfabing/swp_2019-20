package at.johfab.cduebung;

import java.util.ArrayList;
import java.util.List;

public class Actor {
	private String firstName;
	private String lastName;
	private List<CD> CDs;
	
	public Actor(String firstName, String lastName) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		CDs = new ArrayList<CD>();
	}

	public void addCD(CD cd) {
		CDs.add(cd);
	}

	public List<CD> getAllRecords(){
		return CDs;
	}
	
	public String getName() {
		return firstName +" " + lastName;
	}
}
