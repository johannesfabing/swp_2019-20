package at.johfab.cduebung;

public abstract class A_Playable implements Playable {
	
	protected String title;
	protected int length;

	public String getTitle() {
		return this.title;
	}
	
	public int getLength() {
		return this.length;
	}

}
