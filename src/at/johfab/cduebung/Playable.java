package at.johfab.cduebung;

public interface Playable {
	public void play();
	public String getTitle();
	public int getLength();
}
