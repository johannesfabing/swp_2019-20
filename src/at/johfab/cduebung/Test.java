package at.johfab.cduebung;

import java.util.ArrayList;

public class Test {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		
		Player p1 = new Player("Dolby Atmos IMAX Kinosystem");
		
		CD cd1 = new CD("Ali's Rap Album",new ArrayList<Actor>() {{
			add(new Actor("Ali", "Doganyigit"));
		}},new ArrayList<Song>() {{
			add(new Song("BRUH!", 180));
			add(new Song("I am Sbeve", 125));
			add(new Song("Can't walk.", 160));
		}});
		
		CD cd2 = new CD("Johannes Mumble Rap 1",new ArrayList<Actor>() {{
			add(new Actor("Johannes", "Fabing"));
		}},new ArrayList<Song>() {{
			add(new Song("No No No", 150));
			add(new Song("Gotta go", 145));
			add(new Song("Peace out", 234));
		}});
		
		Playable dvd1 = new DVD("Shrek 1", 3600);
		Playable dvd2 = new DVD("Shrek 2", 7200);
		
		for(Song s:cd1.getSongs()){
			p1.addPlayable(s);
		}
		for(Song s:cd2.getSongs()){
			p1.addPlayable(s);
		}
		
		System.out.println(dvd1.getTitle());
		
		p1.addPlayable(dvd1);
		p1.addPlayable(dvd2);
		
		p1.play("No No No");
		p1.play("BRUH!");
		p1.stop();
		p1.play("BRUH!");
		p1.pause();
		p1.resume();
		p1.stop();
		p1.play("Shrek 1");
	}

}
