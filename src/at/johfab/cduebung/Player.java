package at.johfab.cduebung;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private String model;
	private Playable currentlyplaying;
	private boolean pause;
	private List<Playable> Playables;
	
	public Player(String model) {
		super();
		this.model = model;
		this.Playables = new ArrayList<Playable>();
	}

	public void play(String Playablename) {
		if(currentlyplaying == null) {
			for(Playable p : Playables) {
				if(p.getTitle()==Playablename) {
					System.out.println(p.getTitle());
					currentlyplaying = p;
				}
			}
			if(currentlyplaying != null) {
				currentlyplaying.play();
				System.out.println("Playing "+currentlyplaying.getTitle()+" on the "+model+".");
			}
		}
		else {
			System.out.println("Already playing "+currentlyplaying.getTitle());
		}
	}
	
	public void stop() {
		if(currentlyplaying != null) {
			currentlyplaying = null;
			pause = false;
			System.out.println("Song has been stopped.");
		}
	}
	
	public void pause() {
		if(currentlyplaying != null) {
			if(!pause) {
				pause = true;
				System.out.println("Song has been paused.");
			}
		}
	}
	
	public void resume() {
		if(pause) {
			pause = false;
			System.out.println("Song has been resumed.");
		}
	}
	
	public void addPlayable(Playable Playable) {
		this.Playables.add(Playable);
	}
}
