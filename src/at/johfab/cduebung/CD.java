package at.johfab.cduebung;

import java.util.List;

public class CD {
	private String name;
	private List<Actor> actors;
	private List<Song> songs;
	
	public CD(String name, List<Actor> actors, List<Song> songs) {
		super();
		this.name = name;
		this.actors = actors;
		this.songs = songs;
		for(Song s : songs) {
			s.addCD(this);
		}
		for(Actor a : actors) {
			a.addCD(this);
		}
	}

	public String getName() {
		return name;
	}
	
	public void displayActors() {
		for(Actor a : actors){
			System.out.println(a.getName());
		}
	}
	
	public void displaySongs() {
		for(Song s : songs){
			System.out.println(s.getTitle());
		}
	}
	
	public List<Song> getSongs(){
		return songs;
	}
}
