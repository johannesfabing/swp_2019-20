package at.johfab.cduebung;

public class DVD extends A_Playable {
	
	public DVD(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	
	public void play() {
		System.out.println("Titel: "+title);
		System.out.println("L�nge: "+length);
	}
}