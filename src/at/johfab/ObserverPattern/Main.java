package at.johfab.ObserverPattern;

import at.johfab.ObserverPattern.Lantern;
import at.johfab.ObserverPattern.TrafficLight;
import at.johfab.ObserverPattern.Observable;

public class Main {

	public static void main(String[] args) {
		ManagementSystem s = new ManagementSystem("System 1");
		Component c1 = new Lantern();
		Component c2 = new TrafficLight();
		s.addComponent(c1);
		s.addComponent(c2);
		
		Sensor sensor = new Sensor();
		sensor.addItem((Observable) c1);
		sensor.addItem((Observable) c2);
		
		sensor.informAll();

	}

}
