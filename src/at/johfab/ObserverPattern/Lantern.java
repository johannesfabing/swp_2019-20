package at.johfab.ObserverPattern;

import at.johfab.ObserverPattern.Component;
import at.johfab.ObserverPattern.Observable;

public class Lantern implements Component, Observable{
	
	private boolean isAlive = false;

	@Override
	public boolean isAlive() {
		return this.isAlive;
	}

	@Override
	public void start() {
		isAlive = true;
		
	}

	@Override
	public String inform() {
		return "Lantern has been informed";
	}

}
