package at.johfab.ObserverPattern;

public interface Observer {
	
	public void addItem(Observable o);
	public void informAll();
	
}
