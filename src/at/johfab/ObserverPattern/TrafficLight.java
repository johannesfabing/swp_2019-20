package at.johfab.ObserverPattern;

import at.johfab.ObserverPattern.Component;
import at.johfab.ObserverPattern.Observable;

public class TrafficLight implements Component, Observable{

	private boolean isAlive = false;

	@Override
	public boolean isAlive() {
		return this.isAlive;
	}

	@Override
	public void start() {
		isAlive = true;
		
	}

	@Override
	public String inform() {
		return "TrafficLight has been informed";
	}

	
}
