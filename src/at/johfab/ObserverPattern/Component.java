package at.johfab.ObserverPattern;

public interface Component {
	
	public boolean isAlive();
	public void start();

}
