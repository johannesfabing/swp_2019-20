package at.johfab.ObserverPattern;

import java.util.ArrayList;
import java.util.List;

public class ManagementSystem {
	
	private String name;
	private List<Component> components = new ArrayList<Component>();
	
	public ManagementSystem(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void addComponent(Component c) {
		this.components.add(c);
	}

}
