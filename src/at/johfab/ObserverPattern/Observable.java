package at.johfab.ObserverPattern;

public interface Observable {

	public String inform();
}
