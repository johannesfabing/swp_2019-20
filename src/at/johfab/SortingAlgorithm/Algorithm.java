package at.johfab.SortingAlgorithm;

public interface Algorithm {
    public Integer[] doSort(Integer[] array);
}
