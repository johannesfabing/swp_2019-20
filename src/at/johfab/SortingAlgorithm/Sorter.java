package at.johfab.SortingAlgorithm;

import java.util.Random;

public class Sorter {

    private Algorithm algorithm;

    public Sorter(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Integer[] getRandomArray(int size, int randomsize) {
        Integer[] array = new Integer[size];
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            array[i]=r.nextInt(randomsize);
        }
        return array;
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public Integer[] doSort(Integer[] array) {
        return algorithm.doSort(array);
    }
}
