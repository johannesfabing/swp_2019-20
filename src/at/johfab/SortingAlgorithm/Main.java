package at.johfab.SortingAlgorithm;

public class Main {
    public Main() {
    }

        public static void main(String[] args) {
        Sorter s = new Sorter(new Bubble());
        Integer[] unsorted = s.getRandomArray(50000,1000);
        Integer[] array;
        double start;
        double stop;
        System.out.println("Bubblesort:\n");

        array = unsorted.clone();
        start = System.currentTimeMillis();
        s.doSort(array);
        stop = System.currentTimeMillis();
        /*for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i));
        }*/
        System.out.println("Runtime:"+ ((stop-start))+" ms\n");

        s.setAlgorithm(new Insertion());
        System.out.println("Insertionsort:\n");

        array = unsorted.clone();
        start = System.currentTimeMillis();
        array = s.doSort(array);
        stop = System.currentTimeMillis();
        /*for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i));
        }*/
        System.out.println("Runtime:"+ ((stop-start))+" ms\n");

        s.setAlgorithm(new Selection());
        System.out.println("Selectionsort:\n");

        array = unsorted.clone();
        start = System.currentTimeMillis();
        array = s.doSort(unsorted);
        stop = System.currentTimeMillis();
        /*for (int i = 0; i < array.size(); i++) {
            System.out.println(array.get(i));
        }*/
        System.out.println("Runtime:"+ ((stop-start))+" ms\n");


    }
}