package at.johfab.SortingAlgorithm;

public class Selection implements Algorithm {
    int temp;
    int min;
    int i;

    public Integer[] doSort(Integer[] array) {
        i = 0;
        min = 0;
        do {
            min = i;
            i++;
            for (int j = i; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            temp = array[min];
            array[min] = array[i - 1];
            array[i - 1] = temp;
        } while (min + 1 != i);
        return array;
    }
}
