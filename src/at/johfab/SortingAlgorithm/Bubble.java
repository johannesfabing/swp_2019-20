package at.johfab.SortingAlgorithm;

public class Bubble implements Algorithm {
    int temp;

    public Integer[] doSort(Integer[] array) {
        for (int i = 0;i<array.length;i++){
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    temp = array[j];
                    array[j]=array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        return array;
    }
}
