package at.johfab.baumschule;

import java.util.List;

public class ProductionArea {
	public ProductionArea(List<Tree> trees, String name, int size) {
		super();
		this.trees = trees;
		this.name = name;
		this.size = size;
	}

	private List<Tree> trees;
	private String name;
	private int size;
	
	public List<Tree> getAllTrees(){
		return trees;
	}
	
	public void fertilizeAllTrees() {
		for (Tree tree : trees) {
			tree.fertilize();
		}
	}

	public List<Tree> getTrees() {
		return trees;
	}

	public String getName() {
		return name;
	}

	public int getSize() {
		return size;
	}
}
