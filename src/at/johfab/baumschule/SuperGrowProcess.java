package at.johfab.baumschule;

public class SuperGrowProcess implements FertilizeProcess {
	
	private String name = "SuperGrow";
	public void fertilize() {
		System.out.println(name);
	}
	public String getName() {
		return name;
	}
}
