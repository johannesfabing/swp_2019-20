package at.johfab.baumschule;

public class TopGreenProcess implements FertilizeProcess {
	public String name = "TopGreen";
	public void fertilize() {
		System.out.println(name);
	}
	public String getName() {
		return name;
	}
}
