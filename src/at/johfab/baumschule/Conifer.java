package at.johfab.baumschule;

public class Conifer extends Tree {
	public Conifer(int maxSize, int maxDiameter, FertilizeProcess fertilizer) {
		super(maxSize, maxDiameter, fertilizer);
	}

	public void fertilize() {
		getFertilizeProcess().fertilize();
	}
}
