package at.johfab.baumschule;

public abstract class Tree {
	public Tree(int maxSize, int maxDiameter, FertilizeProcess fertilizer) {
		super();
		this.maxSize = maxSize;
		this.maxDiameter = maxDiameter;
		this.fertilizer = fertilizer;
	}

	private int maxSize;
	private int maxDiameter;
	private FertilizeProcess fertilizer;
	
	public void fertilize() {
		
	}
	
	public int getMaxSize() {
		return maxSize;
	}

	public int getMaxDiameter() {
		return maxDiameter;
	}
	
	public FertilizeProcess getFertilizeProcess() {
		return fertilizer;
	}
}
