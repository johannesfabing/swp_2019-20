package at.johfab.baumschule;

public class LeafTree extends Tree {
	public LeafTree(int maxSize, int maxDiameter, FertilizeProcess fertilizer) {
		super(maxSize, maxDiameter, fertilizer);
	}

	public void fertilize() {
		getFertilizeProcess().fertilize();
	}
}
