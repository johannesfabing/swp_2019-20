package at.johfab.baumschule;

import java.util.ArrayList;
import java.util.Random;

public class Test {

	public static void main(String[] args) {
		
		ArrayList<Tree> trees1;
		trees1 = new ArrayList<Tree>();
		
		for(int i = 0; i<20;i++) {
			Random r = new Random();
			FertilizeProcess f1;
			if(r.nextBoolean()) {
				f1 = new SuperGrowProcess();
			}else {
				f1 = new TopGreenProcess();
			}
			
			if(r.nextBoolean()) {
				trees1.add(new Conifer(r.nextInt(30),r.nextInt(50),f1));
			}else {
				trees1.add(new LeafTree(r.nextInt(30),r.nextInt(50),f1));
			}
		}
		
		ProductionArea area1 = new ProductionArea(trees1, "area1", 10);
		
		area1.fertilizeAllTrees();
	}

}
