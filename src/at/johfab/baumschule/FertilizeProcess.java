package at.johfab.baumschule;

public interface FertilizeProcess {
	public void fertilize();
	public String getName();
}
