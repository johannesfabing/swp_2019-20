package at.johfab.Encryption;

public class Main {
    public static void main(String[] args) {
        Cryptor c = new Cryptor(new Caesar());

        String Message = "Hallo!";

        Message=c.encrypt(Message);

        System.out.println(Message);

        Message=c.decrypt(Message);

        System.out.println(Message);
    }
}