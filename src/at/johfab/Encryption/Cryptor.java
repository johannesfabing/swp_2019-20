package at.johfab.Encryption;

import java.util.Random;

public class Cryptor {

    private Algorithm algorithm;

    public Cryptor(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public void setAlgorithm(Algorithm algorithm) {
        this.algorithm = algorithm;
    }

    public String encrypt(String string) {
        return algorithm.encrypt(string);
    }

    public String decrypt(String string) {
        return algorithm.decrypt(string);
    }
}
