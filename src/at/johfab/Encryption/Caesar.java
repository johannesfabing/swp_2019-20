package at.johfab.Encryption;

public class Caesar implements Algorithm {
    int offset = 2;
    int[] range;
    char[] chars;

    public String encrypt(String string){
        chars=string.toCharArray();
        for(int i = 0; i<chars.length;i++){
            range = getRange(chars[i]);
            if(chars[i]+offset<=range[1]){
                chars[i]+=offset;
            }else{
                chars[i]-=range[1]-(offset+range[0]-1);
            }
        }


        return new String(chars);
    }

    public String decrypt(String string){
        chars=string.toCharArray();
        for(int i = 0; i<chars.length;i++){
            range = getRange(chars[i]);
            if(chars[i]-offset>=range[0]){
                chars[i]-=offset;
            }else{
                chars[i]+=range[1]-(offset+range[0]-1);
            }
        }
        return new String(chars);
    }

    private int[] getRange(char c){
        if(c>=65&&c<=90){
            return new int[] {65,90};
        }
        if(c>=97&&c<=122){
            return new int[] {97,122};
        }
        return new int[] {33,126};
    }
}
