package at.johfab.Encryption;

public interface Algorithm {
    public String encrypt(String string);
    public String decrypt(String string);
}
