package at.johfab.auction_bidding;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        final int[] temp = {0};
        String rawdata = "100,160,C,100,C,115,C,119,C,121,C,144,C,154,C,157,G,158,C,171,C,179,C,194,C,206,C,214,C,227,C,229,C,231,C,298";

        Bidding b = new Bidding(Integer.parseInt(List.of(rawdata.split(",")).get(0)),Integer.parseInt(List.of(rawdata.split(",")).get(1)));

        List<String> data = new ArrayList<>(List.of(rawdata.split(",")));
        data.remove(0);
        data.remove(0);
        List<String[]> bidder = new ArrayList<>();

        for(int i = 0;i<data.size();i++){
            if (i!=0&&(i+1) % 2 == 0){
                bidder.add(new String[]{data.get(i), data.get(i-1)});
            }
        }

        System.out.print("-,"+b.getPrice()+",");
        for(String[] n : bidder){
            b.calculatePrice(n[1],Integer.parseInt(n[0]));
            if(temp[0] != b.getPrice()){
                System.out.print(b.getMaxbidder()+","+b.getPrice()+",");
            }
            if(b.getSofortbuy() == b.getPrice()){
                break;
            }
            temp[0] = b.getPrice();
        }
    }
}

