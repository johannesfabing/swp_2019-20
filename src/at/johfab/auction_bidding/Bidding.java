package at.johfab.auction_bidding;

public class Bidding {
    private int sofortbuy;
    private int maxbid;
    private String maxbidder;
    private int price;

    public Bidding(int price, int sofortbuy) {
        this.price = price;
        this.sofortbuy = sofortbuy;
        maxbid = price;
    }

    public String getMaxbidder(){
        return maxbidder;
    }

    public int getPrice(){
        return price;
    }

    public int getSofortbuy() { return sofortbuy; }

    public void calculatePrice(String bidder, int maxbid){
        if(maxbid > this.maxbid){
            if(this.maxbidder==null){
                this.maxbidder = bidder;
            }
            if(this.maxbidder.equals(bidder)){}else{
                this.price = this.maxbid + 1;
            }
            this.maxbid = maxbid;
            this.maxbidder = bidder;
        }else if(maxbid == this.maxbid){
            this.price = maxbid;
            if(this.maxbidder==null){
                this.maxbidder = bidder;
            }
        }else if(maxbid >= this.price){
            this.price = maxbid + 1;
        }
        if(this.price >= sofortbuy && sofortbuy > 0) {
            this.price = sofortbuy;
        }
    }

}
