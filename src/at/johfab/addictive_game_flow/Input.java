package at.johfab.addictive_game_flow;

import java.util.Arrays;
import java.util.stream.IntStream;

public class Input {
    private static final Integer row = 0; //Row Position in Data
    private static final Integer col = 1; //Col Position in Data
    private static final Integer count = 2; //PositionCount in Data
    private Integer[] data;

    public Input(String rawdata) {
        data = Arrays.stream(rawdata.split(" "))
                .map(Object::toString)
                .map(Integer::valueOf)
                .toArray(Integer[]::new);
    }

    public Integer[] getData() {
        return data;
    }

    public int getRows(){
        return data[row];
    }

    public int getColumns(){
        return data[col];
    }

    public Integer[][] getPositions(){
        Integer[] temp = Arrays.copyOfRange(data, count +1, count +1+data[count]*2);

        return IntStream.iterate(0, i -> i + 2)
                    .limit((long) Math.ceil((double) temp.length/2))
                    .mapToObj(j -> Arrays.copyOfRange(temp, j, j + 2 > temp.length ? temp.length : j + 2))
                    .toArray(Integer[][]::new);
    }
}
