package at.johfab.addictive_game_flow;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private int[][] Board;
    private Point[] points;
    private Path[] paths;

    public Board(int rows, int cols, Integer[][] positions) {
        List<Point> temp = new ArrayList<Point>();
        points = new Point[positions.length];
        Board = new int[][]{new int[rows],new int[cols]};
        for(int  i = 0; i < positions.length/2;i++){
            points[i] = new Point(this, positions[i][0],positions[i][1]);
        }

        paths = new Path[points.length/2];

        for(int i = 0; i < points.length/2; i++){
            temp.clear();
            for(int j = 1; j <= points.length; j++){
                if(points[j].getColor() == j){
                    temp.add(points[j]);
                }
            }
            paths[i] = new Path(temp.get(0),temp.get(1));
        }
    }

    public int[][] getBoard() {
        return Board;
    }

    public int[] getManhattanDistances(){
        int[] temp = new int[paths.length];
        for(int i = 0; i < temp.length; i++){
            temp[i] = paths[i].getManhattanDistance();
        }
        return temp;
    }
}
