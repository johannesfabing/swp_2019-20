package at.johfab.addictive_game_flow;

public class Path {
    Point[] points = new Point[2];

    public Path(Point point1, Point point2) {
        this.points[0] = point1;
        this.points[1] = point2;
    }

    public int getManhattanDistance(){
        return Math.abs(points[0].getRow() - points[1].getRow()) + Math.abs(points[0].getCol() - points[1].getCol());
    }
}
