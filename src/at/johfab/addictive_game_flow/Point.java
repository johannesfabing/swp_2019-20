package at.johfab.addictive_game_flow;

public class Point {
    private static final Integer row = 0; //Row Position in point
    private static final Integer col = 1; //Col Position in point
    private Board b;
    private int color;
    private int[] point = new int[2];

    public Point(Board b, int position, int color) {
        this.b = b;
        this.color = color;
        resolvePosition(b,position);
    }

    public int getRow(){
        return point[row];
    }

    public int getCol(){
        return point[col];
    }

    public int getColor(){
        return color;
    }

    private void resolvePosition(Board b, int position) {
        point[row] = (int)Math.ceil((float)position/b.getBoard()[col].length);
        point[col] = position % b.getBoard()[col].length;
        if(point[col] == 0){ point[col] = b.getBoard()[col].length; }
    }
}
