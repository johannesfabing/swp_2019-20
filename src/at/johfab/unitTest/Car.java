package at.johfab.unitTest;

public class Car {
	
	private float price;
	private float discount;
	private float maxspeed;
	private float weight;
	
	public Car(float price, float discount, float maxspeed, float weight) {
		this.price = price;
		this.discount = discount;
		this.maxspeed = maxspeed;
		this.weight = weight;
		
	}
	
	public float getPrice() {
		return this.price;
	}
	
	public float getPriceWithDiscount(){
		return (this.price - (this.price*this.discount/100));
	}
	
	public float getRealMaxSpeed() {
		if(this.weight > 1000 && this.weight < 2000) {
			return (float) (this.maxspeed*0.9);
		}else if(this.weight > 2000) {
			return (float) (this.maxspeed*0.8);
		}else {
			return this.maxspeed;
		}
	}
	

}
